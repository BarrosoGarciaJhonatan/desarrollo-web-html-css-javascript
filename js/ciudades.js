const endpoint =
  "https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json";
/*fetch(endpoint)
    .then(blob => blob.json())
    .then(data => cities.push(data))*/

const cities = [];

const obtenerCiudades = async () => {
  const api = await fetch(endpoint);
  const data = await api.json();
  await data.map((ciudad) => cities.push(ciudad.city));
  console.log(cities);
};
obtenerCiudades();

const titulo = document.getElementById("titulo");
titulo.innerText = "BUSCADOR DE CIUDADES ";
const resultado = document.getElementById("resultado");
const input = document.getElementById("input");

const { fromEvent } = rxjs;
const { map } = rxjs.operators;
fromEvent(input, "input")
  .pipe(
    map(() => input.value.toLowerCase()),
    map((valor) =>
      cities.filter(
        (ciudad) => ciudad.toLowerCase().indexOf(valor) > -1 && !(valor === "")
      )
    )
  )
  .subscribe((ciudadesFiltradas) => {
    resultado.innerHTML = "";
    for (const ciudad of ciudadesFiltradas) {
      const p = document.createElement("p");
      p.innerText = ciudad;
      resultado.appendChild(p);
    }
  });
