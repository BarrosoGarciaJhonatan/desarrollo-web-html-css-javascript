const globo = document.getElementById("globo");
//const {of} = rxjs;
const { delay, take, expand } = rxjs.operators; //map

of(1)
  .pipe(
    expand((valor) => of(valor + 1).pipe(delay(500))),
    map((valor) => valor * 20 + "px"),
    take(10)
  )
  .subscribe((valorCSS) => {
    globo.style.width = valorCSS;
    globo.style.height = valorCSS;
    globo.style.borderRadius = valorCSS;
  });
