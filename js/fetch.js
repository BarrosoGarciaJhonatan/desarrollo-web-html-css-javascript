const datos1 = "../data/estados-municipios.json";
const datos2 = "../data/estados.json";
const datos3 = "../data/paises.json";
//Programar dos peticiones de carga de archivos  en paralelo

const { forkJoin, of } = rxjs;
const { fromFetch } = rxjs.fetch;
const { switchMap, catchError } = rxjs.operators; // map

const cargardatos = (url) => {
  return fromFetch(url).pipe(
    switchMap((respuesta) => {
      if (respuesta.ok) {
        return respuesta.json();
      } else {
        return of({ error: true, mensaje: "Error " + respuesta.status });
      }
    }),
    catchError((error) => of({ error: true, mensaje: error.message }))
  );
};
forkJoin({
  datos1: cargardatos(datos1),
  datos2: cargardatos(datos2),
  datos3: cargardatos(datos3),
}).subscribe(console.log);
