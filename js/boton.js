const control = document.getElementById("control");
const mensaje = document.getElementById("mensaje");
const boton = document.getElementById("boton");
const { BehaviorSubject } = rxjs; //fromEvent
const { shareReplay, startWith } = rxjs.operators;

fromEvent(control, "change").subscribe(() => {
  boton.disabled = control.checked;
  if (control.checked) {
    boton.className = "btn-disabled";
    mensaje.innerHTML = "Activar botón";
  } else {
    boton.className = "btn";
    mensaje.innerHTML = "Desactivar botón";
  }
});
