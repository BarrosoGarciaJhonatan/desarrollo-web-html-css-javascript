const barra = document.getElementById("barra");
const progreso = document.getElementById("progreso");

const { timer } = rxjs;
// const{map, take} = rxjs.operators;
timer(100, 100)
  .pipe(
    map((i) => i + "%"),
    take(101)
  )
  .subscribe((valor) => {
    console.log(valor);
    barra.style.width = valor;
    progreso.innerHTML = "" + valor;
  });
