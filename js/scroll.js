const contenedor = document.getElementById("contenedor");
const { filter } = rxjs.operators;

let element;

fromEvent(document, "scroll")
  .pipe(
    map(() => {
      const alturaDocumento = document.documentElement.scrollHeight;
      const alturaVentana = document.documentElement.clientHeight;
      const scrollTop = document.documentElement.scrollTop;
      element = document.createElement("img");
      element.src =
        "https://i.pinimg.com/originals/53/47/95/534795f1264d4bbce0c4dc986f26da87.gif";
      return scrollTop + alturaVentana === alturaDocumento;
    }),
    filter((valor) => valor === true)
  )
  .subscribe(() => contenedor.appendChild(element));
